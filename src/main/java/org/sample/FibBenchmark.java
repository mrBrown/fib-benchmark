package org.sample;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.*;

@Fork(1)
@BenchmarkMode( {Mode.AverageTime})
@Measurement(iterations = 5, time = 1)
@State(Scope.Benchmark)
@Threads(1)
@Warmup(iterations = 10, time = 2)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class FibBenchmark {

    @Param( {"10", "100", "10000", "1000000"})
    int n;

    private int tailRecursiveFib(int n, int a, int b) {
        if (n == 1) {
            return b;
        }
        return tailRecursiveFib(n - 1, b, a + b);
    }

    private int tailRecursiveFib(int n) {
        if (n == 0) {
            return 0;
        }
        return tailRecursiveFib(n, 0, 1);
    }

    private int recursiveFib(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }

        return recursiveFib(n - 2) + recursiveFib(n - 1);
    }

    private int iterativeFib(int n) {
        int a = 0;
        int b = 1;
        if (n == 0) {
            return a;
        }
        for (int i = 2; i <= n; i++) {
            int c = a + b;
            a = b;
            b = c;
        }
        return b;
    }

    @Benchmark
    public int iterativeFib() {
        return iterativeFib(n);
    }


    @Benchmark
    public int tailRecursiveFib() {
        return tailRecursiveFib(n);
    }

    //@Benchmark
    public int recursiveFib() {
        if (n > 10) {
            throw new IllegalArgumentException("n to big: " + n);
        }
        return recursiveFib(n);
    }

}
