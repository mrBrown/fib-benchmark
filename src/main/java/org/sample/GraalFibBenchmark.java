package org.sample;


import org.openjdk.jmh.annotations.Fork;

@Fork(jvmArgsPrepend = {"-XX:+UnlockExperimentalVMOptions","-XX:+EnableJVMCI", "-XX:+UseJVMCICompiler"})
public class GraalFibBenchmark extends FibBenchmark {

}
